import QtQuick 2.0
import QtQuick.LocalStorage 2.0

Item {
    property var database

    Component.onCompleted: {
        database = LocalStorage.openDatabaseSync("project", "1.0");
        database.transaction(function(client) {
            client.executeSql("CREATE TABLE IF NOT EXISTS task_groups(id INTEGER PRIMARY KEY AUTOINCREMENT, data TEXT NOT NULL)");
        });
    }

    function save(text) {
        database.transaction(function(client) {
            client.executeSql("INSERT INTO task_groups(data) VALUES(?)", [text]);
        });
    }

    function removeById(id) {
        database.transaction(function(client) {
            client.executeSql("DELETE FROM task_groups WHERE id = ?", [id]);
        });
    }

    function findAll(callback, groupId) {
        database.readTransaction(function(client) {
            var result = client.executeSql("SELECT * FROM task_groups");
            callback(result.rows);
        });
    }
}
