import QtQuick 2.0
import QtQuick.LocalStorage 2.0

Item {
    property var database

    Component.onCompleted: {
        database = LocalStorage.openDatabaseSync("project", "1.0");
        database.transaction(function(client) {
            client.executeSql("CREATE TABLE IF NOT EXISTS tasks(id INTEGER PRIMARY KEY AUTOINCREMENT, group_id INTEGER, data TEXT NOT NULL, date TEXT, status BOOLEAN)");
        });
    }

    function save(group_id, text, date) {
        database.transaction(function(client) {
            client.executeSql("INSERT INTO tasks(group_id, data, date, status) VALUES(?, ?, ?, false)", [group_id, text, date]);
        });
    }

    function updateStatus(id, status) {
        database.transaction(function(client) {
            client.executeSql("UPDATE tasks SET status = ? WHERE id = ?", [status, id]);
        });
    }

    function removeById(id) {
        database.transaction(function(client) {
            client.executeSql("DELETE FROM tasks WHERE id = ?", [id]);
        });
    }

    function findAllByGroupId(callback) {
        database.readTransaction(function(client) {
            var result = client.executeSql("SELECT * FROM tasks WHERE group_id = ?", [groupId]);
            callback(result.rows);
        });
    }
}
