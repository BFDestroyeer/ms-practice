import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    property int groupId
    property string groupName
    property bool removeMode: false

    TaskRepository {
            id: repository
        }
    SilicaListView {
        width: parent.width
        height: parent.height
        model: ListModel {
            id: model
        }
        header: PageHeader {
            title: groupName
        }

        section.property: "date"
        section.delegate: BackgroundItem {
                    PageHeader {
                        title: section
                    }
        }

        delegate: TextSwitch {
           id: textSwitch
           width: parent.width
           text: model.data
           checked: model.status
           height: 96
           onClicked: {
               if (!removeMode) {
                   repository.updateStatus(model.id, textSwitch.checked);
               } else {
                   repository.removeById(model.id);
               }
               updateTasks();
           }
        }
        PullDownMenu {
            MenuItem {
                text: "Add task"
                onClicked: {
                    addTaskDialog.open()
                }
            }
            MenuItem {
                id: removeModeItem
                text: "Enable remove mode"
                onClicked: {
                    if (removeMode) {
                        removeModeItem.text = "Enable remove mode"
                        removeMode = false
                    } else {
                        removeModeItem.text = "Disable remove mode"
                        removeMode = true
                    }
                }
            }
        }
    }

    Dialog {
        id: addTaskDialog
        Column {
            spacing: 12
            width: page.width
            DialogHeader {
                title: "Add task"
            }
            TextField {
                id: taskName
                label: "task name"
            }
            Label {
                id: taskDate
            }
            Button {
                text: "Set date"
                onClicked: {
                    datePickerDialog.open()
                }
            }
            DatePickerDialog {
                id: datePickerDialog
                onAccepted: taskDate.text = dateText
            }
        }
        onAccepted: {
            if (taskName.text != "" && taskDate.text != "") {
                repository.save(groupId, taskName.text, taskDate.text)
                updateTasks()
            }
        }
    }

    function updateTasks() {
        model.clear();
        repository.findAllByGroupId(function(tasks) {
            for (var i = 0; i < tasks.length; i++) {
                var task = tasks.item(i);
                model.append({
                                 id: task.id,
                                 data: task.data,
                                 date: task.date,
                                 status: task.status
                             })
            }
        }, groupId)
    }

    Component.onCompleted: updateTasks()
}
