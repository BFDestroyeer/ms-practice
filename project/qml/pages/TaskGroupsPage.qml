import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    TaskGroupRepository {
            id: repository
        }

    SilicaListView {
        anchors.fill: parent

        model: ListModel {
            id: model
        }

        header: PageHeader {
            title: "Task groups"
        }

        delegate: ListItem {
            Label {
                id: label
                text: model.data
                anchors.centerIn: parent
            }
            menu: ContextMenu {
                MenuItem {
                    text: "Open group"
                    onClicked: {
                        pageStack.animatorPush(Qt.resolvedUrl("TasksPage.qml"), {"groupId": model.id, "groupName": model.data})
                    }
                }
                MenuItem {
                    text: "Remove group"
                    onClicked: {
                        repository.removeById(model.id)
                        updateTaskGroups()
                    }
                }
            }
        }
        PullDownMenu {
            MenuItem {
                text: "Add task group"
                onClicked: {
                    addGroupDialog.open()
                }
            }
        }
    }

    Dialog {
        id: addGroupDialog
        Column {
            spacing: 12
            width: page.width
            DialogHeader {
                title: "Add tasks group"
            }
            TextField {
                id: groupName
                label: "Group name"
            }
        }
        onAccepted: {
            if (groupName.text != "") {
                repository.save(groupName.text)
                updateTaskGroups()
            }
        }
    }

    function updateTaskGroups() {
        model.clear();
        repository.findAll(function(groups) {
            for (var i = 0; i < groups.length; i++) {
                var group = groups.item(i);
                model.append({
                                 id: group.id,
                                 data: group.data
                             })
            }
        })
    }


    Component.onCompleted: updateTaskGroups()
}
