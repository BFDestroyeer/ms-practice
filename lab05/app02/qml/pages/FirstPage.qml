import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    Column {
        Label {
            text: "First page"
        }

        Button {
            text: "Add page"
            onClicked: {
                pageStack.pushAttached(Qt.resolvedUrl("SecondPage.qml"))
            }
        }

        Button {
            text: "Remove page"
            onClicked: {
                pageStack.popAttached()
            }
        }
    }
}
