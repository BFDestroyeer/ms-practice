import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    Column {
       Label {
           text: "Page"
       }

        Button {
            text: "Next"
            onClicked: {
                pageStack.pushAttached(Qt.resolvedUrl("FirstPage.qml"))

            }
        }

        Button {
            text: "Back"
            onClicked: {
                pageStack.popAttached()
            }
        }

        Label {
            id: stackLabel
            text: "Stack depth: " + pageStack.depth
        }
    }
}
