import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    SilicaFlickable {
        anchors.fill: parent

        PullDownMenu {
            MenuItem {
                text: "Item1"
                onClicked: {
                    outputText.text = text
                }
            }
            MenuItem {
                text: "Item2"
                onClicked: {
                    outputText.text = text
                }
            }
        }
        PushUpMenu {
            MenuItem {
                text: "Item3"
                onClicked: {
                    outputText.text = text
                }
            }
            MenuItem {
                text: "Item4"
                onClicked: {
                    outputText.text = text
                }
            }
        }

        Column {
            width: page.width
            PageHeader {
                title: "Filckable"
            }

            Text {
                id: outputText
                color: "white"
            }
        }
    }
}
