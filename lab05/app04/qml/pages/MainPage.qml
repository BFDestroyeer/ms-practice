import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    Column {
        Button {
           text: "ListView"
           onClicked: {
               pageStack.push(Qt.resolvedUrl("ListViewDemo.qml"))
           }
        }
        Button {
           text: "WebView"
           onClicked: {
               pageStack.push(Qt.resolvedUrl("WebViewDemo.qml"))
           }
        }
        Button {
           text: "SlideshowView"
           onClicked: {
               pageStack.push(Qt.resolvedUrl("SlideshowViewDemo.qml"))
           }
        }
        Button {
           text: "Flickable"
           onClicked: {
               pageStack.push(Qt.resolvedUrl("FlickableDemo.qml"))
           }
        }
        Button {
           text: "ContextMenu"
           onClicked: {
               pageStack.push(Qt.resolvedUrl("ContextMenuDemo.qml"))
           }
        }
    }
}
