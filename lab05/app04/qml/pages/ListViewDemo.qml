import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    SilicaListView {
        anchors.fill: parent
        delegate: BackgroundItem {
            Label {
                text: name
            }
        }
        section.property: "date"
        section.delegate: BackgroundItem {
            PageHeader {
                title: section
            }
        }

        model: ListModel {
            ListElement {
                name: "Do A";
                date: "01.01.2021"
            }
            ListElement {
                name: "Do B";
                date: "02.01.2021"
            }
            ListElement {
                name: "Do C";
                date: "02.01.2021"
            }
        }

    }
}
