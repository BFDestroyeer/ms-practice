import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    SilicaListView {
        anchors.fill: parent

        model: ListModel {
            id: listModel
            ListElement {
                name: "Element1";
            }
            ListElement {
                name: "Element2";
            }
            ListElement {
                name: "Element3";
            }
        }

        delegate: ListItem {

            Label {
                id: label
                text: model.name
                anchors.centerIn: parent
            }

            menu: ContextMenu {
                MenuItem {
                    text: "Item1"
                    onClicked: console.log(model.index + " " + text)
                }
                MenuItem {
                    text: "Item2"
                    onClicked: console.log(model.index + " " + text)
                }
            }
        }
    }
}
