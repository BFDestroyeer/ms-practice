import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    SlideshowView {
        anchors.fill: parent
        delegate: Rectangle {
            width: parent.itemWidth
            height: parent.height
            Text {
                anchors.centerIn: parent
                text: date + " " + name
            }
        }

        model: ListModel {
            ListElement {
                name: "Do A";
                date: "01.01.2021"
            }
            ListElement {
                name: "Do B";
                date: "02.01.2021"
            }
            ListElement {
                name: "Do C";
                date: "02.01.2021"
            }
        }

    }
}
