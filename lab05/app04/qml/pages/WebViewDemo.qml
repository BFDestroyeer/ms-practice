import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    SilicaWebView {
        anchors.fill: parent
        header: PageHeader {
            title: "google.com"
        }
        url: "https://google.com"
    }
}
