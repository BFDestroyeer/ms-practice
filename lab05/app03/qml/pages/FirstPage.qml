import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    Column {
        // Text dialog
        TextField {
            id: outputTextField
        }

        Button {
            text: "Text dialog"
            onClicked: {
                textDialog.open()
            }
        }

        Dialog {
            id: textDialog
            Column {
                width: page.width
                DialogHeader {}
                TextField {
                    id: inputTextField
                }
            }
            onAccepted: {
                outputTextField.text = inputTextField.text
            }
        }

        // Date dialog
        TextField {
            id: outputDateField
        }

        Button {
            text: "Date dialog"
            onClicked: {
                dateDialog.open()
            }
        }

        DatePickerDialog {
            id: dateDialog
            onAccepted: {
                outputDateField.text = dateText
            }
        }

        // Time dialog
        TextField {
            id: outputTimeField
        }

        Button {
            text: "Time dialog"
            onClicked: {
                timeDialog.open()
            }
        }

        TimePickerDialog {
            id: timeDialog
            onAccepted: {
                outputTimeField.text = timeText
            }
        }
    }
}
