import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    NotesRepository {
        id: repository
    }

    ListModel {
        id: model
    }

    Column {
        anchors.fill: parent
        TextField {
            id: field
            placeholderText: "..."
        }
        Button {
            anchors.horizontalCenter: parent.horizontalCenter
            text: "Add"
            onClicked: {
                if (!field.text) {
                    return;
                }
                repository.save(field.text)
                updateNotes();
            }
        }
        SilicaListView {
            width: parent.width
            height: parent.height
            model: model
            delegate: Button {
               width: parent.width
               text: model.data
               height: 96
               onClicked: {
                   repository.removeById(model.id);
                   updateNotes();
               }
            }
        }
    }

    function updateNotes() {
        model.clear();
        repository.findAll(function(notes) {
            for (var i = 0; i < notes.length; i++) {
                var note = notes.item(i);
                model.append({
                                 id: note.id,
                                 data: note.data
                             })
            }
        })
    }

    Component.onCompleted: updateNotes()
}
