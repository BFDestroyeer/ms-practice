import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page
    property int count: 0

    ListModel {
        id: listModel
    }
    Column {
        anchors.fill: parent

        Button {
            id: button
            anchors.horizontalCenter: parent.horizontalCenter
            text: "add"
            onClicked: {
                listModel.append({text: "Item" + (listModel.count + 1)})
            }
        }
        SilicaListView {
            width: parent.width
            height: parent.height
            model: listModel
            delegate: Rectangle {
                SecondaryButton {
                    anchors.fill: parent
                    onClicked: {
                        listModel.remove(model.index)
                    }
                }
                width: parent.width
                height: 64
                color: "white"
                Text {
                    anchors.centerIn: parent
                    text: model.text
                }
            }
        }
    }
}
