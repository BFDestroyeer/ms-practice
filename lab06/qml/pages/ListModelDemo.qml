import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    ListModel {
        id: listModel
        ListElement { text: "White"; backgroundColor: "white"; textColor: "black" }
        ListElement { text: "Black"; backgroundColor: "black"; textColor: "white"}
        ListElement { text: "Blue"; backgroundColor: "blue"; textColor: "red"}
    }
    SilicaListView {
        spacing: 8
        anchors.fill: parent
        model: listModel
        delegate: Rectangle {
            width: parent.width
            height: 64
            color: model.backgroundColor
            Text {
                anchors.centerIn: parent
                text: model.text
                color: model.textColor
            }
        }
    }
}
