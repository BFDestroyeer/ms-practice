import QtQuick 2.0
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0

Page {
    id: page

    Column {
        anchors.fill: parent
        ConfigurationGroup {
            id: configuration
            synchronous: true
        }

        TextField {
            id: textField
            color: "White"
            onTextChanged: {
                configuration.setValue("/textConfiguration", textField.text)
            }

            Component.onCompleted: {
                textField.text = configuration.value("textField", "Some text")
            }
        }

    }
}
