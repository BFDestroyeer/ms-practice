import QtQuick 2.0
import Sailfish.Silica 1.0
import QtQuick.XmlListModel 2.0

Page {
    id: page

    XmlListModel {
        id: xmlListModel
        query: "/ValCurs/Valute"
        XmlRole { name: "Name"; query: "Name/string()" }
        XmlRole { name: "Value"; query: "Value/string()" }
    }
    SilicaListView {
        anchors.fill: parent
        model: xmlListModel
        delegate: Column {
            Label { text: Name }
            Label { text: Value }
        }
    }
    Component.onCompleted: {
        var request = new XMLHttpRequest();
        request.onreadystatechange = function() {
            if (request.readyState === XMLHttpRequest.DONE) {
                xmlListModel.xml = request.responseText;
            }
        }
        request.open("GET", "https://www.cbr.ru/scripts/XML_daily_eng.asp", true);
        request.send();
    }
}
