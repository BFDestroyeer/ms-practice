import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    property var jsModel: [
        { text: "White", backgroundColor: "white", textColor: "black" },
        { text: "Black", backgroundColor: "black", textColor: "white"},
        { text: "Blue", backgroundColor: "blue", textColor: "red"}
    ]
    SilicaListView {
        spacing: 8
        anchors.fill: parent
        model: jsModel
        delegate: Rectangle {
            width: parent.width
            height: 64
            color: modelData.backgroundColor
            Text {
                anchors.centerIn: parent
                text: modelData.text
                color: modelData.textColor
            }
        }
    }
}
