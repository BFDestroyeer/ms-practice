import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    Column {
        Button {
           text: "ListModel"
           onClicked: {
               pageStack.push(Qt.resolvedUrl("ListModelDemo.qml"))
           }
        }
        Button {
           text: "ListModelAlt"
           onClicked: {
               pageStack.push(Qt.resolvedUrl("ListModelDemoAlt.qml"))
           }
        }
        Button {
           text: "ListModelJS"
           onClicked: {
               pageStack.push(Qt.resolvedUrl("ListModelDemoJS.qml"))
           }
        }
        Button {
           text: "CurrencyRate"
           onClicked: {
               pageStack.push(Qt.resolvedUrl("CurrencyRate.qml"))
           }
        }
        Button {
           text: "CurrencyRateAlt"
           onClicked: {
               pageStack.push(Qt.resolvedUrl("CurrencyRateAlt.qml"))
           }
        }
        Button {
           text: "Notes"
           onClicked: {
               pageStack.push(Qt.resolvedUrl("Notes.qml"))
           }
        }
        Button {
           text: "Configuration"
           onClicked: {
               pageStack.push(Qt.resolvedUrl("Configuration.qml"))
           }
        }
    }
}
