import QtQuick 2.0
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0

Page {
    id: page

    Column {
        anchors.fill: parent
        ConfigurationValue {
            id: textConfiguration
            key: "/textConfiguration"
            defaultValue: "Some text"
        }
        ConfigurationValue {
            id: switchConfiguration
            key: "/switchConfiguration"
            defaultValue: false
        }

        TextSwitch {
            id: textSwitch
            width: page.width
            text: "Disabled"
            onCheckedChanged: {
                text = (checked ? "Enabled" : "Disabled")
                switchConfiguration.value = textSwitch.checked
            }
            Component.onCompleted: {
                textSwitch.checked = switchConfiguration.value
            }
        }

        TextField {
            id: textField
            color: "White"
            onTextChanged: {
                textConfiguration.value = textField.text
            }

            Component.onCompleted: {
                textField.text = textConfiguration.value
            }
        }

    }
}
