import QtQuick 2.0
import QtQuick.LocalStorage 2.0

Item {
    property var database

    Component.onCompleted: {
        database = LocalStorage.openDatabaseSync("notes", "1.0");
        database.transaction(function(client) {
            client.executeSql("CREATE TABLE IF NOT EXISTS notes(id INTEGER PRIMARY KEY AUTOINCREMENT, data TEXT NOT NULL)");
        });
    }

    function save(text) {
        database.transaction(function(client) {
            client.executeSql("INSERT INTO notes(data) VALUES(?)", [text]);
        });
    }

    function removeById(id) {
        database.transaction(function(client) {
            client.executeSql("DELETE FROM notes WHERE id = ?", [id]);
        });
    }

    function findAll(callback) {
        database.readTransaction(function(client) {
            var result = client.executeSql("SELECT * FROM notes");
            callback(result.rows);
        });
    }
}
