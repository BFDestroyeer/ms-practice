import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    Column {

        Button {
            text: "CounterDemo"
            onClicked: pageStack.animatorPush(Qt.resolvedUrl("CounterDemo.qml"))
        }

        Button {
            text: "WordsDemo"
            onClicked: pageStack.animatorPush(Qt.resolvedUrl("WordsDemo.qml"))
        }

    }
}
