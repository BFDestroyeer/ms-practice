import QtQuick 2.0
import Sailfish.Silica 1.0
import cpp.Words 1.0

Page {
    id: page

    Words {
        id: words
    }
    SilicaFlickable {
        anchors.fill: parent
        contentHeight: column.height

        Column {
            id: column
            width: page.width
            spacing: Theme.paddingLarge

            TextField {
                id: text_field
                width: parent.width
                placeholderText: "..."
            }

            Button {
                text: "Push"
                anchors.horizontalCenter: parent.horizontalCenter
                onClicked: {
                    words.push(text_field.text);
                    text_field.text = "";
                }
            }
            Button {
                text: "Pop"
                anchors.horizontalCenter: parent.horizontalCenter
                onClicked: words.pop()
            }

            Label {
                id: label
                width: page.width
                wrapMode: "Wrap"
                text: words.list
            }
        }
    }
}
