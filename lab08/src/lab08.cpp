#ifdef QT_QML_DEBUG
#include <QtQuick>
#endif

#include <sailfishapp.h>

#include "counter.h"
#include "words.h"

int main(int argc, char *argv[])
{
    {
        const QMetaObject metaObject = Counter::staticMetaObject;
        QObject *instance = metaObject.newInstance();
        QObject::connect(instance, SIGNAL(countChanged()), instance, SLOT(print()));
        metaObject.invokeMethod(instance, "increment");
        metaObject.invokeMethod(instance, "increment");
        metaObject.invokeMethod(instance, "reset");
    }
    QGuiApplication *application = SailfishApp::application(argc, argv);
    QQuickView *view = SailfishApp::createView();
    view->setSource(SailfishApp::pathTo("qml/lab08.qml"));
    qmlRegisterType<Counter>("cpp.Counter", 1, 0, "Counter");
    qmlRegisterType<Words>("cpp.Words", 1, 0, "Words");
    view->showFullScreen();
    return application->exec();
}
