#ifndef COUNTER_H
#define COUNTER_H

#include <QDebug>
#include <QObject>

class Counter : public QObject {
    Q_OBJECT
    Q_PROPERTY(int count READ getCount WRITE setCounter NOTIFY countChanged)

private:
    int count;

public:
    Q_INVOKABLE Counter() : QObject() {
        this->count = 0;
    }

    Q_INVOKABLE void increment() {
        this->count++;
        emit countChanged();
    }

    Q_INVOKABLE void reset() {
        this->count = 0;
        emit countChanged();
    }

    int getCount() {
        return this->count;
    }

    void setCounter(int counter) {
        this->count = counter;
        emit countChanged();
    }

public slots:
    void print() {
        qDebug() << "Count: " << this->count;
    }

signals:
    void countChanged();
};

#endif // COUNTER_H
