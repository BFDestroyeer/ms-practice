#ifndef WORDS_H
#define WORDS_H

#include <QObject>
#include <QString>
#include <QStringList>

class Words : public QObject {
  Q_OBJECT
  Q_PROPERTY(QString list READ string NOTIFY updated)

private:
  QStringList words = QStringList();

public:
  Words() : QObject() {
  }

  Q_INVOKABLE void push(QString string) {
    this->words.append(string.toLower());
    emit updated();
  }

  Q_INVOKABLE void pop() {
    this->words.removeLast();
    emit updated();
  }

  QString string() {
    QString text = this->words.join(", ");
    return text.replace(0, 1, text[0].toUpper());
  }

signals:
  void updated();
};

#endif // WORDS_H
