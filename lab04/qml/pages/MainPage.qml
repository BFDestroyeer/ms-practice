import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    Column {
        anchors.fill: parent

        Button {
            text: "Buttons"
            anchors.horizontalCenter: parent.horizontalCenter
            onClicked: {
                pageStack.animatorPush(Qt.resolvedUrl("Buttons.qml"))
            }
        }

        Button {
            text: "Pickers"
            anchors.horizontalCenter: parent.horizontalCenter
            onClicked: {
                pageStack.animatorPush(Qt.resolvedUrl("Pickers.qml"))
            }
        }

        Button {
            text: "ComboboxAndOthers"
            anchors.horizontalCenter: parent.horizontalCenter
            onClicked: {
                pageStack.animatorPush(Qt.resolvedUrl("ComboBoxAndOthers.qml"))
            }
        }
    }
}
