import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    Column {
        ComboBox {
             width: page.width
             label: "Choose me"
             menu: ContextMenu {
                 MenuItem { text: "John" }
                 MenuItem { text: "Jack" }
                 MenuItem { text: "Jill" }
                 MenuItem { text: "Jane" }
             }
             onCurrentIndexChanged: {
                 console.log(currentItem)
             }
         }

         TextSwitch {
             width: page.width
             text: "Disabled"
             onCheckedChanged: {
                 text = (checked ? "Enabled" : "Disabled")
             }
         }

         Slider {
             id: slider

             width: 400
             minimumValue: 0
             maximumValue: 400
             value: 0
             stepSize: 1
         }

         Label {
             text: slider.value
         }
    }
}
