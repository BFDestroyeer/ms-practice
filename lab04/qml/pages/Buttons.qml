import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    Column {
            Button {
                text: "Button"
                onClicked: {
                    down = true
                }
            }

            Button {
                text: "Button"
                onPressed: {
                    textField.text = "Pressed"
                }

                onReleased:{
                    textField.text = "Released"
                }
            }

            TextField {
                id: textField
                text: ""
            }

            ValueButton {
                label: "Counter"
                value: counterValue

                property int counterValue: 0

                onClicked: {
                    counterValue++
                }
            }
        }
}
