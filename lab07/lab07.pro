# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = lab07

CONFIG += sailfishapp

SOURCES += src/lab07.cpp

DISTFILES += qml/lab07.qml \
    qml/cover/CoverPage.qml \
    qml/pages/CustomComponent.qml \
    qml/pages/CustomComponentDemo.qml \
    qml/pages/FirstPage.qml \
    qml/pages/PageStackDemo.qml \
    qml/pages/StopWatchDemo.qml \
    qml/pages/TextDemo.qml \
    qml/pages/TimeSection.qml \
    qml/pages/TrafficLightComponent.qml \
    qml/pages/TrafficLightComponentDemo.qml \
    qml/pages/TrafficLightDemo.qml \
    rpm/lab07.changes.in \
    rpm/lab07.changes.run.in \
    rpm/lab07.spec \
    rpm/lab07.yaml \
    translations/*.ts \
    lab07.desktop

SAILFISHAPP_ICONS = 86x86 108x108 128x128 172x172

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n

# German translation is enabled as an example. If you aren't
# planning to localize your app, remember to comment out the
# following TRANSLATIONS line. And also do not forget to
# modify the localized app name in the the .desktop file.
TRANSLATIONS += translations/lab07-de.ts
