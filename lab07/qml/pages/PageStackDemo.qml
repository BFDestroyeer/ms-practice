import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    property int previousDepth: 2
    property int pagesPushed: 0
    property int pagesPopped: 0

    function stackDepthChangeHandler() {
        if (typeof PageStack === "undefined") {
            return
        } else if (previousDepth > pageStack.depth) {
            pagesPopped++;
        } else if (previousDepth < pageStack.depth) {
            pagesPushed++;
        }
        previousDepth = pageStack.depth;
    }

    Component.onCompleted: {
        pageStack.depthChanged.connect(stackDepthChangeHandler);
    }

    Column {
        id: column
        anchors.fill: parent;
        width: page.width
        Button {
            text: "Pop"
            onClicked: pageStack.pop()
        }
        Button {
            text: "Push"
            onClicked: pageStack.push(Qt.resolvedUrl("PageStackDemo.qml"))
        }
        Label {
            text: "Depth: " + pageStack.depth
        }
    }
}
