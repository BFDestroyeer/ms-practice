import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    Column {
        Button {
           text: "TrafficLightDemo"
           onClicked: {
               pageStack.push(Qt.resolvedUrl("TrafficLightDemo.qml"))
           }
        }
        Button {
           text: "TextDemo"
           onClicked: {
               pageStack.push(Qt.resolvedUrl("TextDemo.qml"))
           }
        }
        Button {
           text: "TrafficLightComponentDemo"
           onClicked: {
               pageStack.push(Qt.resolvedUrl("TrafficLightComponent.qml"))
           }
        }
        Button {
           text: "CustomComponentDemo"
           onClicked: {
               pageStack.push(Qt.resolvedUrl("CustomComponentDemo.qml"))
           }
        }
        Button {
           text: "StopWatchDemo"
           onClicked: {
               pageStack.push(Qt.resolvedUrl("StopWatchDemo.qml"))
           }
        }
        Button {
           text: "PageStackDemo"
           onClicked: {
               pageStack.push(Qt.resolvedUrl("PageStackDemo.qml"))
           }
        }
    }
}
