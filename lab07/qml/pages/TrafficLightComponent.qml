import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page
    Component.onCompleted: animation.restart()

    Column {
        id: trafficLight
        spacing: 16
        anchors.fill: parent
        property int counter: 0

        SequentialAnimation {
            id: animation
            loops: Animation.Infinite
            PropertyAnimation {
                target: trafficLight
                properties: "counter"
                from: 0
                to: 6
                duration:  6000
            }
        }

        state: "default"

        states: [
            State {
                name: "default"
            },
            State {
                name: "red"
                when: trafficLight.counter >= 0 && trafficLight.counter < 2
                PropertyChanges {
                    target: red
                    color: "Red"
                }
            },
            State {
                name: "yellow"
                when: trafficLight.counter >= 2 && trafficLight.counter < 4
                PropertyChanges {
                    target: yellow
                    color: "Yellow"
                }
            },
            State {
                name: "green"
                when: trafficLight.counter >= 4 && trafficLight.counter < 6
                PropertyChanges {
                    target: green
                    color: "Green"
                }
            }
        ]

        Rectangle {
            id: red
            color: "black"
            width: 128
            height: 128
            radius: 128
        }
        Rectangle {
            id: yellow
            color: "black"
            width: 128
            height: 128
            radius: 128
        }
        Rectangle {
            id: green
            color: "black"
            width: 128
            height: 128
            radius: 128
        }
    }
}
