import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    Text {
        anchors.horizontalCenter: page.horizontalCenter
        id: animatedText
        text: "Hello, world!"
        font.pixelSize: 64
        color: "Blue"
        state: "default"

        MouseArea {
            anchors.fill: parent
            onPressed: parent.state = "animation"
            onReleased: parent.state = "default"
        }

        states: [
            State {
                name: "default"
            },
            State {
                name: "animation"
                PropertyChanges {
                    target: animatedText
                    color: "green"
                    rotation: 180
                }
            }
        ]
        transitions: [
            Transition {
                from: "default"
                to: "animation"
                ParallelAnimation {
                    PropertyAnimation {
                        target: animatedText
                        property: "y"
                        to: page.height
                        duration: 3000
                    }
                }
            },
            Transition {
                from: "animation"
                to: "default"
                ParallelAnimation {
                    PropertyAnimation {
                        target: animatedText
                        property: "y"
                        to: 0
                        duration: 3000
                    }
                }
            }
        ]
    }
}
