import QtQuick 2.0
import Box2D 1.0
import Sailfish.Silica 1.0

Body {
    id: wall

    property bool isBottom: false

    signal onBeginContact()

    bodyType: Body.Static
    fixtures: Box {
        anchors.fill: parent
        friction: 1.0

        onBeginContact: {
            if (wall.isBottom == true) {
                gamePage.balls--
                if (balls == 0) {
                    gamePage.showResultScreen()
                }
                gamePage.moveBallToInitialPosition()
            }
        }
    }
}
