import QtQuick 2.0
import Box2D 1.0
import Sailfish.Silica 1.0

Page {
    id: resultPage

    property int score;

    Column {
        anchors.fill: parent

        Button {
            height: 96
            width: parent.width
            text: "Easy"
            onClicked: {
                pageStack.animatorReplace(Qt.resolvedUrl("GamePage.qml"), {"difficulty": 1})
            }
        }
        Button {
            height: 96
            width: parent.width
            text: "Hard"
            onClicked: {
                pageStack.animatorReplace(Qt.resolvedUrl("GamePage.qml"), {"difficulty": 2})
            }
        }
    }
}
