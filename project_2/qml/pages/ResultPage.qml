import QtQuick 2.0
import Box2D 1.0
import Sailfish.Silica 1.0

Page {
    id: resultPage

    property int score;

    Column {
        anchors.fill: parent

        Label {
            anchors.horizontalCenter: parent.horizontalCenter
            text: "Game over"
            font.pixelSize: 96
        }
        Label {
            anchors.horizontalCenter: parent.horizontalCenter
            text: "Your score: " + resultPage.score
            font.pixelSize: 72
        }
        Button {
            height: 96
            width: parent.width
            text: "Back to menu"
            onClicked: {
                pageStack.animatorReplace(Qt.resolvedUrl("MainPage.qml"), {})
            }
        }
    }
}
