import QtQuick 2.0
import Box2D 1.0
import Sailfish.Silica 1.0

Body {
    id: brick

    height: 48
    width: 120

    property string brickColor
    property int brickScore

    bodyType: Body.Static
    fixtures: Box {
        anchors.fill: parent
        friction: 1.0
        density: 2

        onEndContact: {
            gamePage.score += brick.brickScore * gamePage.difficulty
            gamePage.bricks--
            removeBrick()
            if (gamePage.bricks == 0) {
                gamePage.showResultScreen()
            }
        }
    }

    Rectangle {
        id: brickRectangle
        anchors {
            fill: parent
        }
        color: brick.brickColor
    }

    function removeBrick() {
        brick.destroy()
    }
}
