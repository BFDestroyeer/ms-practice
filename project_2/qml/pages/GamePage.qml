import QtQuick 2.0
import Box2D 1.0
import Sailfish.Silica 1.0

Page {
    id: gamePage

    property int balls: 3;
    property int bricks: 16;
    property int score: 0;
    property int difficulty: 1

    World {
        id: world
        anchors.fill: parent

        gravity.x: 0
        gravity.y: 0

        Wall {
            id: wallTop

            height: 8
            width: parent.width
            anchors {
                bottom: parent.top
            }
        }

        Wall {
            id: wallBottom
            isBottom: true

            height: 8
            width: parent.width
            anchors {
                top: parent.bottom
            }
        }

        Wall {
            id: wallLeft

            width: 8
            height: parent.height
            anchors {
                left: parent.right
            }
        }

        Wall {
            id: wallRight

            width: 8
            height: parent.height
            anchors {
                right: parent.left
            }
        }

        Ball {
            id: ball
            x: parent.width / 2
            y: parent.height * (2 / 3)
        }

        Platform {
            y: parent.height - 128
        }
    }

    Label {
        text: "Score: " + score
        anchors {
            right: parent.right
            bottom: parent.bottom
        }
    }

    Label {
        text: "Balls: " + balls
        anchors {
            left: parent.left
            bottom: parent.bottom
        }
    }

    MouseArea {
        height: parent.height / 2
        width: parent.width
        onClicked: {
            ball.applyLinearImpulse(Qt.point(3000 * gamePage.difficulty, -3000 * gamePage.difficulty), Qt.point(ball.x, ball.y))
            if (!ball.active) {
                ball.active = true
            }
        }
    }

    function setupBricks() {
        var colors = ["Red", "Orange", "Green", "Blue"]
        var component = Qt.createComponent("Brick.qml");
        for (var row = 0; row < 4; row++) {
            for (var column = 0; column < 4; column++) {
                component.createObject(world, {
                                           "x": (gamePage.width / 4) * column,
                                           "y": 200 + 100 * row,
                                           "brickColor": colors[row],
                                           "brickScore": 4 - row
                                       })
            }
        }
    }

    function moveBallToInitialPosition() {
        ball.x = gamePage.width / 2
        ball.y = gamePage.height * (2 / 3)
    }

    function showResultScreen() {
        pageStack.animatorReplace(Qt.resolvedUrl("ResultPage.qml"), {"score": gamePage.score})
    }

    Component.onCompleted: setupBricks()
}
