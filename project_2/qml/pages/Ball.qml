import QtQuick 2.0
import Box2D 1.0
import Sailfish.Silica 1.0

Body {
    id: ball

    fixedRotation: false
    sleepingAllowed: false

    fixtures: Circle {
        id: circle
        anchors.fill: parent
        radius: 32
        density: 2
        friction: 0
        restitution: 1.01
    }

    Rectangle {
        anchors.centerIn: parent
        color: "White"
        width: circle.radius * 2
        height: circle.radius * 2
        radius: circle.radius
    }
}
