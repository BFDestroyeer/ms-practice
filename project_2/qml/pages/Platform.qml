import QtQuick 2.0
import Box2D 1.0
import Sailfish.Silica 1.0

Body {
    id: platform

    width: 196
    height: 48

    bodyType: Body.Static
    fixtures: Box {
        id: platformBox
        anchors.fill: parent
        friction: 100
        density: 3000;
    }

    Rectangle {
        id: platformRectangle
        anchors.fill: parent
    }

    MouseArea {
        anchors.fill: parent
        drag.target: platform
        drag.axis: Drag.XAxis
        drag.minimumX: 0
        drag.maximumX: gamePage.width - platform.width
    }
}
