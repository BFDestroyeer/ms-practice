# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = project_2

CONFIG += sailfishapp

SOURCES += src/project_2.cpp

include(qml-box2d/box2d-static.pri)

DISTFILES += qml/project_2.qml \
    qml/cover/CoverPage.qml \
    qml/pages/Ball.qml \
    qml/pages/Brick.qml \
    qml/pages/GamePage.qml \
    qml/pages/MainPage.qml \
    qml/pages/Platform.qml \
    qml/pages/ResultPage.qml \
    qml/pages/Wall.qml \
    rpm/project_2.changes.in \
    rpm/project_2.changes.run.in \
    rpm/project_2.spec \
    rpm/project_2.yaml \
    translations/*.ts \
    project_2.desktop

SAILFISHAPP_ICONS = 86x86 108x108 128x128 172x172

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n

# German translation is enabled as an example. If you aren't
# planning to localize your app, remember to comment out the
# following TRANSLATIONS line. And also do not forget to
# modify the localized app name in the the .desktop file.
TRANSLATIONS += translations/project_2-de.ts
