import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    property int rectangleSize: 200

    Rectangle {
        id: rectangle
        height: page.rectangleSize
        width: page.rectangleSize
        color: "red"

        anchors.horizontalCenter: parent.horizontalCenter

        ParallelAnimation {
            id: animation
            PropertyAnimation {
                target: rectangle
                properties: "scale"
                from: 1.0
                to: 2.0
                duration: 2000
            }
            PropertyAnimation {
                target: rectangle
                properties: "y"
                from: 0
                to: page.height
                duration: 2000
            }
        }
    }
    onClicked: animation.restart()
}
