import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    property int rectangleSize: 200

    Rectangle {
        id: rectangleRed

        height: page.rectangleSize
        width: page.rectangleSize
        color: "red"
    }

    Rectangle {
        id: rectangleGreen

        height: page.rectangleSize
        width: page.rectangleSize
        color: "green"

        anchors.left: rectangleRed.right
        anchors.verticalCenter: rectangleRed.bottom
    }

    Rectangle {
        id: rectangleBlue

        height: page.rectangleSize
        width: page.rectangleSize
        color: "blue"

        anchors.verticalCenter: rectangleGreen.top
        anchors.horizontalCenter: rectangleGreen.right

        Text {
            text: "Квадрат"
            color: "white"

            anchors.centerIn: parent
        }
    }
}
