import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    Column {
        anchors.fill: parent

        Button {
            text: "Basic rectangles"
            anchors.horizontalCenter: parent.horizontalCenter
            onClicked: {
                pageStack.animatorPush(Qt.resolvedUrl("BasicRectangles.qml"))
            }
        }

        Button {
            text: "Advanced rectangles"
            anchors.horizontalCenter: parent.horizontalCenter
            onClicked: {
                pageStack.animatorPush(Qt.resolvedUrl("AdvancedRectangles.qml"))
            }
        }

        Button {
            text: "Grid rectangles"
            anchors.horizontalCenter: parent.horizontalCenter
            onClicked: {
                pageStack.animatorPush(Qt.resolvedUrl("GridRectangles.qml"))
            }
        }

        Button {
            text: "Transforms rectangles"
            anchors.horizontalCenter: parent.horizontalCenter
            onClicked: {
                pageStack.animatorPush(Qt.resolvedUrl("TransformRectangles.qml"))
            }
        }

        Button {
            text: "Animation rectangles"
            anchors.horizontalCenter: parent.horizontalCenter
            onClicked: {
                pageStack.animatorPush(Qt.resolvedUrl("AnimationRectangles.qml"))
            }
        }

        Button {
            text: "Dialog"
            anchors.horizontalCenter: parent.horizontalCenter
            onClicked: {
                dialog.open()
            }
        }

        Dialog {
            id: dialog
            DialogHeader {}

            Row {
                anchors.top: parent.top
                anchors.topMargin: 200

                TextField {
                    id: textFieldA

                    placeholderText: "0"
                    width: 300
                }

                TextField {
                    id: textFieldB

                    placeholderText: "0"
                    width: 300

                    anchors.left: textFieldA.right
                }
            }

            onAccepted: {
                console.log("SUMM: " + (parseInt(textFieldA.text) + parseInt(textFieldB.text)))
            }
        }
    }
}
