import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    property int rectangleSize: 200

    Grid {
        spacing: 10
        columns: 3
        rows: 2

        Rectangle {
            height: page.rectangleSize
            width: page.rectangleSize
            color: "red"
        }

        Rectangle {
            height: page.rectangleSize
            width: page.rectangleSize
            color: "green"
        }

        Rectangle {
            height: page.rectangleSize
            width: page.rectangleSize
            color: "blue"
        }

        Rectangle {
            height: page.rectangleSize
            width: page.rectangleSize
            color: "magenta"
        }

        Item {
            height: page.rectangleSize
            width: page.rectangleSize
        }

        Rectangle {
            height: page.rectangleSize
            width: page.rectangleSize
            color: "black"
        }
    }
}
