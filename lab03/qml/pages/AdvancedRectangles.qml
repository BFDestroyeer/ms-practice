import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    property int rectangleSize: 200

    Row {
        spacing: 10

        Column {
            spacing: 10

            Rectangle {
                height: page.rectangleSize
                width: page.rectangleSize
                color: "red"
            }

            Rectangle {
                height: page.rectangleSize
                width: page.rectangleSize
                color: "magenta"
            }
        }

        Column {
            spacing: 10

            Rectangle {
                height: page.rectangleSize
                width: page.rectangleSize
                color: "green"
            }
        }

        Column {
            spacing: 10

            Rectangle {
                height: page.rectangleSize
                width: page.rectangleSize
                color: "blue"
            }
            Rectangle {
                height: page.rectangleSize
                width: page.rectangleSize
                color: "black"
            }
        }
    }


}
