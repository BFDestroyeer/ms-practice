import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    property int rectangleSize: 200

    Rectangle {
        height: page.rectangleSize
        width: page.rectangleSize
        color: "black"

        Text {
            text: "A"
            color: "white"

            anchors.centerIn: parent
        }
    }

    Rectangle {
        height: page.rectangleSize
        width: page.rectangleSize
        color: "black"

        Text {
            text: "B"
            color: "white"

            anchors.centerIn: parent
        }

        transform: [
            Scale {
                xScale: 0.5
            },
            Rotation {
                angle: 45
            },
            Translate {
                x: page.rectangleSize * 2
                y: page.rectangleSize * 0.1
            }
        ]
    }

}
