import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: mainPage

    allowedOrientations: Orientation.All

    Column {
        Label {
            id: label_counter
            text: value
            horizontalAlignment: "AlignHCenter"

            property int value: 0
        }

        Button {
            id: button_increment
            text: "Add"

            onClicked: {
                label_counter.value++
            }
        }
    }
}
