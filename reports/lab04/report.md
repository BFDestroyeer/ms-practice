\<Титульный лист>

# Постановка задачи

## Цель

Научиться применять типовые элементы интерфейса Sailfish OS.

## Шаги

1. Создать текстовое поле для ввода числа с заголовком и подсказкой
2. Создать кнопку, которая будет сохранять визуально нажатое состояние, после того, как пользователь нажал на неё один раз
3. Создать кнопку и поле с текстом. Поле с текстом должно отображать нажата ли кнопка или нет выводом текста “Нажата” или “Отпущена”

4. Создать кнопку со значением, которая будет отображать количество нажатий на неё

5. Создать селектор даты, который будет отображать выбранную дату в консоли
6. Создать селектор времени, который будет отображать выбранное время в консоли
7. Создать поле с выпадающим списком, позволяющее выбрать строку из списка. Результат выбора отобразить в консоли
8. Создать переключатель с текстом, в тексте отобразить состояние переключателя “Включен” или “Выключен”
9.  Создать ползунок и поле с текстом. Поле с текстом должно отображать текущее значение ползунка



## Руководство пользователя

1. Запустите приложение **lab03**. На экране отобразится меню с кнопками, чтобы перейти к нужному разделу нажмите соответствующую кнопку.
![](media/user01.png)
2. Шаги cо второго по четвёртый представлены в пункте **Buttons**. Верхняя кнопка визуально сохраняет нажатое состояние, состояние средней кнопки отображается в поле ниже, самая нижняя кнопка показывает число нажатий на ней.
   ![](media/user02.png)
3. Шаги cо пятого по шестой представлены в пункте **Pickers**. В форме сверху можно выбрать дату, а в форме снизу, перемещая импровизированные стрелки можно выбрать время.
   ![](media/user03.png)
4. Шаги cо седьмого по девятый представлены в пункте **ComboboxAndOthers**. Сверху представлен выпадающий список, по середине расположен переключатель, аналогичный привычным radio button, в низу расположен ползунок, значение которого отображается в поле под ним.
   ![](media/user04.png)


# Руководство программиста

Для выполнения лабораторной работы были использованы следующие компоненты пакета Sailfish SDK, включающего Qt Quick:

* **ValueButton** - кнопка со значением
* **DatePicker** - форма выбора даты
* **TimePicker** -  форма выбора времени:
* **ComboBox** - выпадающий список
* **TextSwitch** - текстовый переключатель
* **Slider** - ползунок


# Приложение

## Buttons.qml

```qml
import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    Column {
            Button {
                text: "Button"
                onClicked: {
                    down = true
                }
            }

            Button {
                text: "Button"
                onPressed: {
                    textField.text = "Pressed"
                }

                onReleased:{
                    textField.text = "Released"
                }
            }

            TextField {
                id: textField
                text: ""
            }

            ValueButton {
                label: "Counter"
                value: counterValue

                property int counterValue: 0

                onClicked: {
                    counterValue++
                }
            }
        }
}

```

## ComboBoxAndOthers.qml

```qml
import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    Column {
        ComboBox {
             width: page.width
             label: "Choose me"
             menu: ContextMenu {
                 MenuItem { text: "John" }
                 MenuItem { text: "Jack" }
                 MenuItem { text: "Jill" }
                 MenuItem { text: "Jane" }
             }
             onCurrentIndexChanged: {
                 console.log(currentItem)
             }
         }

         TextSwitch {
             width: page.width
             text: "Disabled"
             onCheckedChanged: {
                 text = (checked ? "Enabled" : "Disabled")
             }
         }

         Slider {
             id: slider

             width: 400
             minimumValue: 0
             maximumValue: 400
             value: 0
             stepSize: 1
         }

         Label {
             text: slider.value
         }
    }
}

```

## MainPage.qml

```qml
import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    Column {
        anchors.fill: parent

        Button {
            text: "Buttons"
            anchors.horizontalCenter: parent.horizontalCenter
            onClicked: {
                pageStack.animatorPush(Qt.resolvedUrl("Buttons.qml"))
            }
        }

        Button {
            text: "Pickers"
            anchors.horizontalCenter: parent.horizontalCenter
            onClicked: {
                pageStack.animatorPush(Qt.resolvedUrl("Pickers.qml"))
            }
        }

        Button {
            text: "ComboboxAndOthers"
            anchors.horizontalCenter: parent.horizontalCenter
            onClicked: {
                pageStack.animatorPush(Qt.resolvedUrl("ComboBoxAndOthers.qml"))
            }
        }
    }
}

```

## Pickers.qml

```qml
import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    Column {
        DatePicker {
            date: new Date()
            monthYearVisible: true

            onDateChanged: {
                console.log(date)
            }
        }

        TimePicker {
            width: page.width
            hour: 0
            minute: 0
            onTimeChanged: {
                console.log(hour + ":" + minute)
            }
        }
    }
}

```
