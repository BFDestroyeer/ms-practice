\<Титульный лист>

# Постановка задачи

## Цель

Научиться создавать пользовательский интерфейс конфигурируемый состояниями, реализовывать анимированные переходы при смене состояний и создавать собственные QML компоненты.

## Шаги

1. Создать приложение, отображающее светофор. На экране должно присутствовать 3 разноцветных сигнала, которые загораются и гаснут в том же порядке, что и сигналы светофора. Сделать автоматическую смену состояний.
2. Доработать задание 1 так, чтобы во время зеленого сигнала светофора из одного конца экрана в другой плавно двигалась иконка человечка.
3. Создать приложение, отображающее строку текста вверху экрана. При нажатии на текст он должен плавно перемещаться вниз экрана, поворачивать на 180 градусов и менять цвет. Когда нажатие прекращается, он должен так же плавно возвращаться в исходное положение.
4. Выделить сигналы светофора из задания 1 в отдельный компонент и использовать его.
5. Создать QML компонент со свойством по умолчанию, который берет значение свойства text любого объявленного внутри него объекта и создает Button с тем же текстом. Добавить возможность задавать цвет кнопки при объявлении компонента.
6. Создать приложение-секундомер. На экране должны отображаться значения часов, минут и секунд. Секундомер запускается по сигналу кнопки, при повторном нажатии секундомер останавливается. Для отображения часов, минут и секунд использовать собственные QML компоненты.
7. Добавить обработчик сигналов PageStack, подсчитывающий количество добавленных и удаленных страниц в PageStack.


# Руководство пользователя

1. Запустите приложение **lab07**. На экране отобразится меню с кнопками, чтобы перейти к нужному разделу нажмите соответствующую кнопку.

![](media/user01.png)

2. Шаги с первого по второй представлены в пункте **TrafficLight**.

![](media/user02.png)

3. Третий шаг представлен в пункте **TextDemo**. При  нажатии на текст, он перевернётся, поменяет цвет и начнёт движение вниз, если отпустить кнопку то текст вернётся к изначальному положению.

![](media/user03.png)

4. Четвёртый шаг представлен в пункте **TrafficLightComponent.**

![](media/user04.png)

5. Пятый шаг представлен в пункте **CustomComponentDemo**. На странице имеется компонент, создающий кнопку с текстом из внутреннего компонента **Text**.

![](media/user05.png)

6. Шестой шаг представлен в пункту **StopWatchDemo**. При нажатии на кнопку секундомер начнёт считать время, при повторном нажатии он остановится.

![](media/user06.png)

7. Седьмой шаг представлен в пункте **PageStackDemo**. При нажатии на кнопку **Push** текущая страница добавится в стек, а при нажатии на **Pop** удалится, счётчик показывает глубину стека.

![](media/user07.png)

# Руководство программиста

Для выполнения лабораторной работы были использованы следующие компоненты пакета Sailfish SDK включающего Qt Quick:

* **Item** - базовый тип для создания собственных посредством наследования от базового.
* **State** - компонент, описывающий состояний объекта и условий перехода между ними.
  * **PropertyChanges** - изменение свойства.
  * **StateChangeScript** - выполнение кода.

* **Transition** -  компонент, описывающий перехода между состояниями:
    * **ParallelAnimation** - позволяет запускать анимации параллельно.
    * **SequentialAnimation** - позволяет запускать анимации последовательно.


# Приложение

### CustomComponent.qml

```qml
import QtQuick 2.0
import Sailfish.Silica 1.0

Item {
    default property  var item
    property var color
    Button {
        text: parent.item.text
        color: parent.color
    }
}

```

### CustomComponentDemo.qml

```qml
import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    CustomComponent {
        Text {
            text: "Here we go!"
        }
        color: "green"
    }
}

```

### FirstPage.qml

```qml
import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    Column {
        Button {
           text: "TrafficLightDemo"
           onClicked: {
               pageStack.push(Qt.resolvedUrl("TrafficLightDemo.qml"))
           }
        }
        Button {
           text: "TextDemo"
           onClicked: {
               pageStack.push(Qt.resolvedUrl("TextDemo.qml"))
           }
        }
        Button {
           text: "TrafficLightComponentDemo"
           onClicked: {
               pageStack.push(Qt.resolvedUrl("TrafficLightComponent.qml"))
           }
        }
        Button {
           text: "CustomComponentDemo"
           onClicked: {
               pageStack.push(Qt.resolvedUrl("CustomComponentDemo.qml"))
           }
        }
        Button {
           text: "StopWatchDemo"
           onClicked: {
               pageStack.push(Qt.resolvedUrl("StopWatchDemo.qml"))
           }
        }
        Button {
           text: "PageStackDemo"
           onClicked: {
               pageStack.push(Qt.resolvedUrl("PageStackDemo.qml"))
           }
        }
    }
}

```



### PageStackDemo.qml

```qml
import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    property int previousDepth: 2
    property int pagesPushed: 0
    property int pagesPopped: 0

    function stackDepthChangeHandler() {
        if (typeof PageStack === "undefined") {
            return
        } else if (previousDepth > pageStack.depth) {
            pagesPopped++;
        } else if (previousDepth < pageStack.depth) {
            pagesPushed++;
        }
        previousDepth = pageStack.depth;
    }

    Component.onCompleted: {
        pageStack.depthChanged.connect(stackDepthChangeHandler);
    }

    Column {
        id: column
        anchors.fill: parent;
        width: page.width
        Button {
            text: "Pop"
            onClicked: pageStack.pop()
        }
        Button {
            text: "Push"
            onClicked: pageStack.push(Qt.resolvedUrl("PageStackDemo.qml"))
        }
        Label {
            text: "Depth: " + pageStack.depth
        }
    }
}

```

### TextDemo.qml

```qml
import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    Text {
        anchors.horizontalCenter: page.horizontalCenter
        id: animatedText
        text: "Hello, world!"
        font.pixelSize: 64
        color: "Blue"
        state: "default"

        MouseArea {
            anchors.fill: parent
            onPressed: parent.state = "animation"
            onReleased: parent.state = "default"
        }

        states: [
            State {
                name: "default"
            },
            State {
                name: "animation"
                PropertyChanges {
                    target: animatedText
                    color: "green"
                    rotation: 180
                }
            }
        ]
        transitions: [
            Transition {
                from: "default"
                to: "animation"
                ParallelAnimation {
                    PropertyAnimation {
                        target: animatedText
                        property: "y"
                        to: page.height
                        duration: 3000
                    }
                }
            },
            Transition {
                from: "animation"
                to: "default"
                ParallelAnimation {
                    PropertyAnimation {
                        target: animatedText
                        property: "y"
                        to: 0
                        duration: 3000
                    }
                }
            }
        ]
    }
}

```

### TrafficLightComponent.qml

```qml
import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page
    Component.onCompleted: animation.restart()

    Column {
        id: trafficLight
        spacing: 16
        anchors.fill: parent
        property int counter: 0

        SequentialAnimation {
            id: animation
            loops: Animation.Infinite
            PropertyAnimation {
                target: trafficLight
                properties: "counter"
                from: 0
                to: 6
                duration:  6000
            }
        }

        state: "default"

        states: [
            State {
                name: "default"
            },
            State {
                name: "red"
                when: trafficLight.counter >= 0 && trafficLight.counter < 2
                PropertyChanges {
                    target: red
                    color: "Red"
                }
            },
            State {
                name: "yellow"
                when: trafficLight.counter >= 2 && trafficLight.counter < 4
                PropertyChanges {
                    target: yellow
                    color: "Yellow"
                }
            },
            State {
                name: "green"
                when: trafficLight.counter >= 4 && trafficLight.counter < 6
                PropertyChanges {
                    target: green
                    color: "Green"
                }
            }
        ]

        Rectangle {
            id: red
            color: "black"
            width: 128
            height: 128
            radius: 128
        }
        Rectangle {
            id: yellow
            color: "black"
            width: 128
            height: 128
            radius: 128
        }
        Rectangle {
            id: green
            color: "black"
            width: 128
            height: 128
            radius: 128
        }
    }
}

```

### TrafficLightComponentDemo.qml

```qml
import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    TrafficLightComponent {}
}

```

### TrafficLightDemo.qml

```qml
import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page
    Component.onCompleted: animation.restart()

    Column {
        id: trafficLight
        spacing: 16
        anchors.fill: parent
        property int counter: 0

        SequentialAnimation {
            id: animation
            loops: Animation.Infinite
            PropertyAnimation {
                target: trafficLight
                properties: "counter"
                from: 0
                to: 6
                duration:  6000
            }
        }

        state: "default"

        states: [
            State {
                name: "default"
            },
            State {
                name: "red"
                when: trafficLight.counter >= 0 && trafficLight.counter < 2
                PropertyChanges {
                    target: red
                    color: "Red"
                }
            },
            State {
                name: "yellow"
                when: trafficLight.counter >= 2 && trafficLight.counter < 4
                PropertyChanges {
                    target: yellow
                    color: "Yellow"
                }
            },
            State {
                name: "green"
                when: trafficLight.counter >= 4 && trafficLight.counter < 6
                PropertyChanges {
                    target: green
                    color: "Green"
                }
            }
        ]

        transitions: [
            Transition {
                from: "yellow"
                to: "green"
                PropertyAnimation {
                    target: pedestrian
                    property: "x"
                    from: 0
                    to: page.width - pedestrian.width
                    duration: 2000
                }
            }
        ]

        Rectangle {
            id: red
            color: "black"
            width: 128
            height: 128
            radius: 128
        }
        Rectangle {
            id: yellow
            color: "black"
            width: 128
            height: 128
            radius: 128
        }
        Rectangle {
            id: green
            color: "black"
            width: 128
            height: 128
            radius: 128
        }

        Icon {
            id: pedestrian
            x: 0
            y: page.height - height
            source: "image://theme/icon-m-person"
        }
    }
}

```
