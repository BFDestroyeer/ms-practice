\<Титульный лист>

# Постановка задачи

## Цель

Научиться организовывать многостраничное приложение, использовать контейнеры Silica, вытягиваемые меню и обложку приложения.

## Шаги

1. Создать приложение, которое будет отображать страницу с двумя кнопками “Назад” и “Вперёд”. Первая удалит текущую страницу со стека, вторая добавит новую. Также на экране нужно отображать текущую глубину стека.
2. Создать приложение из двух страниц. Первая страница содержит две кнопки “Добавить страницу” и “Убрать страницу”. Первая кнопка добавит вторую страницу как прикреплённую, вторая кнопка её удалит. На второй странице должна быть кнопка для возврата на первую страницу без закрытия второй.
3. Создать приложение с одной кнопкой и текстовом поле. После нажатия на кнопку отображается диалог для ввода текста. После согласия с результатом введённый текст отображается в текстовое поле.
4. Создать приложение с одной кнопкой и текстовым полем. После нажатия на кнопку отображается диалог для выбора даты. После согласия с результатом ввода выбранная дата отображается в текстовое поле.
5. Создать приложение с одной кнопкой и текстовым полем. После нажатия на кнопку отображается диалог для выбора времени. После согласия с результатом ввода выбранное время отображается в текстовом поле.
6. Создать приложение со списком SilicaListView, из задач на неделю. Задачи должны содержать дату и описание. В списке задачи группировать по датам.
7. Создать приложение с SilicaWebView для доступа к вашему любимому сайту.
8. Использовать SlideshowView для просмотра и перелистывания задач на неделю. На одном слайде – одна задача.
9.  Создать приложение с вытягиваемыми меню сверху и снизу и текстовым полем. После выбора какого-либо элемента меню, его название отобразить в текстовом поле.
10. Создать приложение со списком и контекстным меню. После выбора элемента контекстного меню отобразить в консоли название выбранного элемента меню и индекс элемента списка.
11. Создать приложение с обложной-счётчиком. На обложке отобразить текущий счёт и две кнопки для добавления единицы к счёту и для сброса счётчика.



# Руководство пользователя

1. Для демонстрации первого шага запустите приложение **app01**.

![](media/user01.png)

2. При нажатии на кнопку **Next** в стек добавится текущая страница, при нажатии **Back** она удалится, чтобы перейти на уровень ниже, нажмите на область справа, чтобы перейти на уровень выше нажмите на область слева.

3. Для демонстрации второго шага запустите приложение **app02**.

![](media/user02.png)

4. При нажатии на кнопку **Add page** добавляется вторая страница как прикреплённая, при нажатии **Remove pack** она удалится, чтобы перейти на вторую страницу, нажмите на область справа.

![](media/user03.png)

5. На второй странице при нажатии на кнопку **Back** произойдёт переход к первой странице.

6. Для демонстрации шагов с третьего по пятый запустите приложение **app03**.
   

![](media/user04.png)

7. Нажатие на одну из кнопок выведет один из трёх диалогов - запроса текста, даты или времени. После ввода получения данных они отобразятся в поле над соответствующим диалогом.

8. Для демонстрации шагов с шестого по одиннадцатый запустите приложение **app04**. На экране отобразится меню с кнопками, чтобы перейти к нужному разделу нажмите соответствующую кнопку.

![](media/user05.png)

9. В пункте **ListView** демонстрируется список задач на неделю.

![](media/user06.png)

10. В пункте **WebView** демонстрируется подсмотрщик сайтов, для примера открыт сайт *google.com*.

![](media/user07.png)

11. В пункте **SlideshowView** демонстрируется список задач на неделю в виде слайд-шоу.

![](media/user08.png)

12. В пункте **Filckable** демонстрируется вытягиваемое сверху и снизу меню, после выбора пункта, его название отобразится в поле

![](media/user09.png)

13. В пункте **ContextMenu** демонстрируется контекстное меню. Для раскрытия меню нажмите на него. Выбранный элемент меню отобразится в консоли

![](media/user10.png)

14. Сверните приложение, чтобы отобразилась его обложка. При нажатии на *плюс*, число на обложке увеличится на один, при нажатии на *крестик* число сбросится.

![](media/user11.png)

# Руководство программиста

В ходе выполнения практической работы были изучены и использованы новые компоненты Sailfish SDK а именно:
* **pageStack** - объект, представляющий собой стек страниц приложения, предоставляет методы для контроля над страницами

* **SilicaListView** - форма, для отображения контента в виде списка

* **SilicaWebView** - форма для отображения интернет страниц

* **SlideshowView** - форма для отображения контента в виде слайд-шоу

## Меню

* **ContextMenu** - контекстное меню
* **MenuItem** - элемент меню
* **MenuLabel** - название элемента меню
* **PullDownMenu** - вытягиваемое вниз меню
* **PullUpMenu** - вытягиваемое вверх меню
## Обложка
* **CoverAction** - действие обложки представимое в виде кнопки на оной
* **CoverActionList** - список действий обложки
* **CoverBackground** - обложка приложения


# Приложение

## app01

### FirstPage.qml

```qml
import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    Column {
       Label {
           text: "Page"
       }

        Button {
            text: "Next"
            onClicked: {
                pageStack.pushAttached(Qt.resolvedUrl("FirstPage.qml"))

            }
        }

        Button {
            text: "Back"
            onClicked: {
                pageStack.popAttached()
            }
        }

        Label {
            id: stackLabel
            text: "Stack depth: " + pageStack.depth
        }
    }
}
```

## app02

### FirstPage.qml

```qml
import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    Column {
        Label {
            text: "First page"
        }

        Button {
            text: "Add page"
            onClicked: {
                pageStack.pushAttached(Qt.resolvedUrl("SecondPage.qml"))
            }
        }

        Button {
            text: "Remove page"
            onClicked: {
                pageStack.popAttached()
            }
        }
    }
}

```

### SecondPage.qml

```qml
import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    Column {
        Label {
            text: "Second page"
        }
        Button {
            text: "Back"
            onClicked: {
                pageStack.navigateBack()
            }
        }
    }
}

```



## app03

### FirstPage.qml

```qml
import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    Column {
        // Text dialog
        TextField {
            id: outputTextField
        }

        Button {
            text: "Text dialog"
            onClicked: {
                textDialog.open()
            }
        }

        Dialog {
            id: textDialog
            Column {
                width: page.width
                DialogHeader {}
                TextField {
                    id: inputTextField
                }
            }
            onAccepted: {
                outputTextField.text = inputTextField.text
            }
        }

        // Date dialog
        TextField {
            id: outputDateField
        }

        Button {
            text: "Date dialog"
            onClicked: {
                dateDialog.open()
            }
        }

        DatePickerDialog {
            id: dateDialog
            onAccepted: {
                outputDateField.text = dateText
            }
        }

        // Time dialog
        TextField {
            id: outputTimeField
        }

        Button {
            text: "Time dialog"
            onClicked: {
                timeDialog.open()
            }
        }

        TimePickerDialog {
            id: timeDialog
            onAccepted: {
                outputTimeField.text = timeText
            }
        }
    }
}

```

## app04

### ContextMenuDemo.qml

```qml
import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    SilicaListView {
        anchors.fill: parent

        model: ListModel {
            id: listModel
            ListElement {
                name: "Element1";
            }
            ListElement {
                name: "Element2";
            }
            ListElement {
                name: "Element3";
            }
        }

        delegate: ListItem {

            Label {
                id: label
                text: model.name
                anchors.centerIn: parent
            }

            menu: ContextMenu {
                MenuItem {
                    text: "Item1"
                    onClicked: console.log(model.index + " " + text)
                }
                MenuItem {
                    text: "Item2"
                    onClicked: console.log(model.index + " " + text)
                }
            }
        }
    }
}

```

### FlickableDemo.qml

```qml
import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    SilicaFlickable {
        anchors.fill: parent

        PullDownMenu {
            MenuItem {
                text: "Item1"
                onClicked: {
                    outputText.text = text
                }
            }
            MenuItem {
                text: "Item2"
                onClicked: {
                    outputText.text = text
                }
            }
        }
        PushUpMenu {
            MenuItem {
                text: "Item3"
                onClicked: {
                    outputText.text = text
                }
            }
            MenuItem {
                text: "Item4"
                onClicked: {
                    outputText.text = text
                }
            }
        }

        Column {
            width: page.width
            PageHeader {
                title: "Filckable"
            }

            Text {
                id: outputText
                color: "white"
            }
        }
    }
}

```

### ListViewDemo.qml

```qml
import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    SilicaListView {
        anchors.fill: parent
        delegate: BackgroundItem {
            Label {
                text: name
            }
        }
        section.property: "date"
        section.delegate: BackgroundItem {
            PageHeader {
                title: section
            }
        }

        model: ListModel {
            ListElement {
                name: "Do A";
                date: "01.01.2021"
            }
            ListElement {
                name: "Do B";
                date: "01.01.2022"
            }
            ListElement {
                name: "Do C";
                date: "01.01.2022"
            }
        }

    }
}

```

### MainPage.qml

```qml
import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    Column {
        Button {
           text: "ListView"
           onClicked: {
               pageStack.push(Qt.resolvedUrl("ListViewDemo.qml"))
           }
        }
        Button {
           text: "WebView"
           onClicked: {
               pageStack.push(Qt.resolvedUrl("WebViewDemo.qml"))
           }
        }
        Button {
           text: "SlideshowView"
           onClicked: {
               pageStack.push(Qt.resolvedUrl("SlideshowViewDemo.qml"))
           }
        }
        Button {
           text: "Flickable"
           onClicked: {
               pageStack.push(Qt.resolvedUrl("FlickableDemo.qml"))
           }
        }
        Button {
           text: "ContextMenu"
           onClicked: {
               pageStack.push(Qt.resolvedUrl("ContextMenuDemo.qml"))
           }
        }
    }
}

```

### SlideshowViewDemo.qml

```qml
import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    SlideshowView {
        anchors.fill: parent
        delegate: Rectangle {
            width: parent.itemWidth
            height: parent.height
            Text {
                anchors.centerIn: parent
                text: date + " " + name
            }
        }

        model: ListModel {
            ListElement {
                name: "Do A";
                date: "01.01.2021"
            }
            ListElement {
                name: "Do B";
                date: "01.01.2022"
            }
            ListElement {
                name: "Do C";
                date: "01.01.2022"
            }
        }

    }
}

```

### WebViewDemo.qml

```qml
import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    SilicaWebView {
        anchors.fill: parent
        header: PageHeader {
            title: "google.com"
        }
        url: "https://google.com"
    }
}

```

### CoverPage.qml

```qml
import QtQuick 2.0
import Sailfish.Silica 1.0

CoverBackground {
    id: cover
    property int counter: 0

    Label {
        anchors.centerIn: parent
        text: counter
    }

    CoverActionList{
        CoverAction {
            iconSource: "image://theme/icon-cover-cancel"
            onTriggered: {
                cover.counter = 0
            }
        }
        CoverAction {
            iconSource: "image://theme/icon-cover-new"
            onTriggered: {
                cover.counter++
            }
        }
    }
}

```

