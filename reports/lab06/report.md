\<Титульный лист>

# Постановка задачи

## Цель

Научиться использовать различные модели для отображения данных в прокручиваемых списках, взаимодействовать с базой данных и управлять настройками приложения.

## Шаги

1. Создать приложение, которое позволяет отображать список из прямоугольников с использованием ListModel. В модели должны настраиваться цвет фона и текста внутри прямоугольника. Текст содержит название цвета фона прямоугольника.
2. Создать приложение, которое позволяет отображать список из прямоугольников. Нажатие на кнопку над списком добавит новый элемент. Нажатие на элемент в списке удалит его из списка. В прямоугольниках должен отображаться порядковый номер, присваиваемый при добавлении в список. При удалении элементов порядковые номера у добавленных прямоугольников  остаются неизменными.
3. Выполнить задание 1 с использованием javascript-модели.
4. Получить и отобразить курсы валют из ресурса ЦБ РФ по адресу http://www.cbr.ru/scripts/XML_daily.asp.
5. Выполнить задание 4 с использованием XMLHttpRequest.
6. Создать приложение, позволяющее добавлять и удалять заметки с использованием базы данных и отображать их в списке. Текстовое поле служит для ввода текста, кнопка для добавления заметки, нажатие на заметку удалит её.
7. Создать приложение с текстовым полем и полем с флажком, значение которых сохраняется в настройках приложения с помощью ConfigurationValue.


# Руководство пользователя

1. Запустите приложение **lab06**. На экране отобразится меню с кнопками, чтобы перейти к нужному разделу нажмите соответствующую кнопку.

![](media/user01.png)

2. Первый и третий шаги демонстрируются в пунктах **ListModel** и **ListModelJS**.

![](media/user02.png)

3. Третий шаг демонстрируется в пункте **ListModelAlt**. Для добавления прямоугольника нажмите **Add**, для удаления кликните по нему.

![](media/user03.png)

4. Четвёртый и пятый шаги демонстрируются в пунктах **CurrencyRate** и **CurrencyRateAlt**

![](media/user04.png)

5. Шестой шаг демонстрируется в пункте **Notes**. Для добавления заметку следует ввести её текст в поле и нажать **Add**

![](media/user05.png)

6. Седьмой шаг демонстрируется в пункте **Configuration**

![](media/user06.png)

# Руководство программиста

В ходе выполнения практической работы были изучены и использованы новые компоненты Sailfish SDK а именно:

* **XmlListModel** - модель списка в формате XML

В рамках **NotesRepository** реализованы функция для взаимодействия с БД:
* `save` - добавить заметку в базу
* `remove` - удалить заметку из таблицы
* `findAll` - получить все заметки из базы

# Приложение

### Configuration.qml

```qml
import QtQuick 2.0
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0

Page {
    id: page

    Column {
        anchors.fill: parent
        ConfigurationValue {
            id: textConfiguration
            key: "/textConfiguration"
            defaultValue: "Some text"
        }
        ConfigurationValue {
            id: switchConfiguration
            key: "/switchConfiguration"
            defaultValue: false
        }

        TextSwitch {
            id: textSwitch
            width: page.width
            text: "Disabled"
            onCheckedChanged: {
                text = (checked ? "Enabled" : "Disabled")
                switchConfiguration.value = textSwitch.checked
            }
            Component.onCompleted: {
                textSwitch.checked = switchConfiguration.value
            }
        }

        TextField {
            id: textField
            color: "White"
            onTextChanged: {
                textConfiguration.value = textField.text
            }

            Component.onCompleted: {
                textField.text = textConfiguration.value
            }
        }

    }
}

```

### CurrencyRate.qml

```qml
import QtQuick 2.0
import Sailfish.Silica 1.0
import QtQuick.XmlListModel 2.0

Page {
    id: page

    XmlListModel {
        id: xmlListModel
        source: "http://www.cbr.ru/scripts/XML_daily_eng.asp"
        query: "/ValCurs/Valute"
        XmlRole { name: "Name"; query: "Name/string()" }
        XmlRole { name: "Value"; query: "Value/string()" }
    }
    SilicaListView {
        anchors.fill: parent
        model: xmlListModel
        delegate: Column {
            Label { text: Name }
            Label { text: Value }
        }
    }
}

```

### CurrencyRateAlt.qml

```qml
import QtQuick 2.0
import Sailfish.Silica 1.0
import QtQuick.XmlListModel 2.0

Page {
    id: page

    XmlListModel {
        id: xmlListModel
        query: "/ValCurs/Valute"
        XmlRole { name: "Name"; query: "Name/string()" }
        XmlRole { name: "Value"; query: "Value/string()" }
    }
    SilicaListView {
        anchors.fill: parent
        model: xmlListModel
        delegate: Column {
            Label { text: Name }
            Label { text: Value }
        }
    }
    Component.onCompleted: {
        var request = new XMLHttpRequest();
        request.onreadystatechange = function() {
            if (request.readyState === XMLHttpRequest.DONE) {
                xmlListModel.xml = request.responseText;
            }
        }
        request.open("GET", "https://www.cbr.ru/scripts/XML_daily_eng.asp", true);
        request.send();
    }
}

```

### ListModelDemo.qml

```qml
import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    ListModel {
        id: listModel
        ListElement { text: "White"; backgroundColor: "white"; textColor: "black" }
        ListElement { text: "Black"; backgroundColor: "black"; textColor: "white"}
        ListElement { text: "Blue"; backgroundColor: "blue"; textColor: "red"}
    }
    SilicaListView {
        spacing: 8
        anchors.fill: parent
        model: listModel
        delegate: Rectangle {
            width: parent.width
            height: 64
            color: model.backgroundColor
            Text {
                anchors.centerIn: parent
                text: model.text
                color: model.textColor
            }
        }
    }
}

```

### ListModelDemoAlt.qml

```qml
import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page
    property int count: 0

    ListModel {
        id: listModel
    }
    Column {
        anchors.fill: parent

        Button {
            id: button
            anchors.horizontalCenter: parent.horizontalCenter
            text: "add"
            onClicked: {
                listModel.append({text: "Item" + (listModel.count + 1)})
            }
        }
        SilicaListView {
            width: parent.width
            height: parent.height
            model: listModel
            delegate: Rectangle {
                SecondaryButton {
                    anchors.fill: parent
                    onClicked: {
                        listModel.remove(model.index)
                    }
                }
                width: parent.width
                height: 64
                color: "white"
                Text {
                    anchors.centerIn: parent
                    text: model.text
                }
            }
        }
    }
}

```

### ListModelDemoJS.qml

```qml
import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    property var jsModel: [
        { text: "White", backgroundColor: "white", textColor: "black" },
        { text: "Black", backgroundColor: "black", textColor: "white"},
        { text: "Blue", backgroundColor: "blue", textColor: "red"}
    ]
    SilicaListView {
        spacing: 8
        anchors.fill: parent
        model: jsModel
        delegate: Rectangle {
            width: parent.width
            height: 64
            color: modelData.backgroundColor
            Text {
                anchors.centerIn: parent
                text: modelData.text
                color: modelData.textColor
            }
        }
    }
}

```

### MainPage.qml

```qml
import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    Column {
        Button {
           text: "ListModel"
           onClicked: {
               pageStack.push(Qt.resolvedUrl("ListModelDemo.qml"))
           }
        }
        Button {
           text: "ListModelAlt"
           onClicked: {
               pageStack.push(Qt.resolvedUrl("ListModelDemoAlt.qml"))
           }
        }
        Button {
           text: "ListModelJS"
           onClicked: {
               pageStack.push(Qt.resolvedUrl("ListModelDemoJS.qml"))
           }
        }
        Button {
           text: "CurrencyRate"
           onClicked: {
               pageStack.push(Qt.resolvedUrl("CurrencyRate.qml"))
           }
        }
        Button {
           text: "CurrencyRateAlt"
           onClicked: {
               pageStack.push(Qt.resolvedUrl("CurrencyRateAlt.qml"))
           }
        }
        Button {
           text: "Notes"
           onClicked: {
               pageStack.push(Qt.resolvedUrl("Notes.qml"))
           }
        }
        Button {
           text: "Configuration"
           onClicked: {
               pageStack.push(Qt.resolvedUrl("Configuration.qml"))
           }
        }
    }
}

```

### Notes.qml

```qml
import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    NotesRepository {
        id: repository
    }

    ListModel {
        id: model
    }

    Column {
        anchors.fill: parent
        TextField {
            id: field
            placeholderText: "..."
        }
        Button {
            anchors.horizontalCenter: parent.horizontalCenter
            text: "Add"
            onClicked: {
                if (!field.text) {
                    return;
                }
                repository.save(field.text)
                updateNotes();
            }
        }
        SilicaListView {
            width: parent.width
            height: parent.height
            model: model
            delegate: Button {
               width: parent.width
               text: model.data
               height: 96
               onClicked: {
                   repository.removeById(model.id);
                   updateNotes();
               }
            }
        }
    }

    function updateNotes() {
        model.clear();
        repository.findAll(function(notes) {
            for (var i = 0; i < notes.length; i++) {
                var note = notes.item(i);
                model.append({
                                 id: note.id,
                                 data: note.data
                             })
            }
        })
    }

    Component.onCompleted: updateNotes()
}

```

### NotesRepository.qml

```qml
import QtQuick 2.0
import QtQuick.LocalStorage 2.0

Item {
    property var database

    Component.onCompleted: {
        database = LocalStorage.openDatabaseSync("notes", "1.0");
        database.transaction(function(client) {
            client.executeSql("CREATE TABLE IF NOT EXISTS notes(id INTEGER PRIMARY KEY AUTOINCREMENT, data TEXT NOT NULL)");
        });
    }

    function save(text) {
        database.transaction(function(client) {
            client.executeSql("INSERT INTO notes(data) VALUES(?)", [text]);
        });
    }

    function removeById(id) {
        database.transaction(function(client) {
            client.executeSql("DELETE FROM notes WHERE id = ?", [id]);
        });
    }

    function findAll(callback) {
        database.readTransaction(function(client) {
            var result = client.executeSql("SELECT * FROM notes");
            callback(result.rows);
        });
    }
}

```
