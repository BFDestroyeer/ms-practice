\<Титульный лист>

# Постановка задачи

## Цель

Научиться использовать C++ классы в QML, научиться писать собственные QML компоненты на языке C++ и использовать их в приложении.

## Шаги

1. Создать класс-счётчик с полем для хранения текущего значения и методами для увеличения значения на единицу и сброса до нуля.
2. Использовать мета-объект класса-счётчика для создания объекта и вызова его методов (использовать функцию main, результат изменения состояния проверять выводом на консоль).
3. Создать приложение с текстовым полем и двумя кнопками. Использовать класс-счётчик в QML: текстовое поле должно отображать текущее значение счётчика, кнопки используются для увеличения значения счётчика на единицу и сброса значения до нуля.
4. Сделать поле со значением счётчика свойством и инициализировать его каким-либо значением при создании объекта в QML.
5. Создать класс, содержащий список из строк. Класс должен содержать методы для добавления строки в список и удаления последней добавленной строки.
6. Создать приложение, позволяющее добавить введённое слово и удалить последнее добавленное с использованием данного класса в QML. Слова сохраняются в нижнем регистре.
7. Реализовать свойство только для чтения, которое позволяет получить список всех строк в виде одной, перечисленных через запятую и использовать это свойство для вывода добавленных строк на экран. Свойство должно моментально реагировать на изменение содержимого списка, первое слово начинается с заглавной буквы.



# Руководство пользователя

1. Запустите приложение **app08**. На экране отобразится меню с кнопками, чтобы перейти к нужному разделу нажмите соответствующую кнопку.

![](media/user01.png)

2. Шаги с первого по четвёртый представлены в пункте **CounterDemo**. При нажатии на кнопку **Add** значение счётчика увеличится, а при нажатии на **Reset** - сбросится.

![](media/user02.png)

3. Шаги с пятого по седьмой представлены в пункте **WordsDemo**. Если ввести в текстовое поле какое-либо слово, после чего нажать **Push** то слово отобразится в списке ниже через запятую, притом первое слово будет записано с заглавной буквы. Если нажать **Pop** то последнее добавленное слово будет удалено.

![](media/user03.png)

# Руководство программиста

В ходе выполнения практической работы были изучены и использованы, новые компоненты Sailfish SDK (а в конкретном случае исключительно входящей в состав библиотеки Qt) а именно:
**QObject** - базовый класс всех мета-объектов Qt. Позволяет использовать следующие макросы:

*  `Q_OBJECT` - позволяет посредством MOC использовать динамические свойства, сигналы и слоты.
* `Q_PROPERTY` - используется для объявления свойств в классах, наследующих QObject. Свойства ведут себя как поля класса, но у них есть дополнительный функционал и набор методов, которые можно указать или активировать чтобы  с ними можно было взаимодействовать из QML, например
  * `getMethod` - метод для считывания значения.
  * `setMethod` - метод для установки значения свойства.
  * `resetMethod` - метод для установки значения свойства по умолчанию.
  * `STORED` - сохраняется ли при сохранении состояния объекта (true).
  * `USER` - редактируется ли свойство пользователем (false).
* Макрос `Q_INVOKABLE` – позволяет вызвать метод класса через через систему Meta-Object System.
### Мета-объекты

* `QMetaObject` - класс, предоставляющий доступ к мента объектам.
  * `newInstance()` – генерирует объект заданного класса.
  * `invokeMethod()` - вызывает метод.

`qmlRegisterType()` - регистрирует тип, чтобы он был доступен в QML.

### Взаимодействие
Взаимодействие мета-объектов осуществляется через систему сигналов/слотов:

* `signals` – событие, при его наступлении вызываются связанные обработчики.

* `slots` – обработчик события.

Связывание сигналов и слотов осуществляется через метод `QObject::connect()`.

### Дополнительные классы

С использованием средств описанных выше были реализованы классы `Counter` и `Words`. Первый представляет собой счётчик и предоставляет интерфейс взаимодействия с собой в QML, второй является обёрткой над списком строк, и предоставляет удобный интерфейс для решения задачи.


# Приложение

Ниже приведен исходный код страниц приложения на языке QML и классов на языке С++.

### lab08.cpp

```cpp
#ifdef QT_QML_DEBUG
#include <QtQuick>
#endif

#include <sailfishapp.h>

#include "counter.h"
#include "words.h"

int main(int argc, char *argv[])
{
    {
        const QMetaObject metaObject = Counter::staticMetaObject;
        QObject *instance = metaObject.newInstance();
        QObject::connect(instance, SIGNAL(countChanged()), instance, SLOT(print()));
        metaObject.invokeMethod(instance, "increment");
        metaObject.invokeMethod(instance, "increment");
        metaObject.invokeMethod(instance, "reset");
    }
    QGuiApplication *application = SailfishApp::application(argc, argv);
    QQuickView *view = SailfishApp::createView();
    view->setSource(SailfishApp::pathTo("qml/lab08.qml"));
    qmlRegisterType<Counter>("cpp.Counter", 1, 0, "Counter");
    qmlRegisterType<Words>("cpp.Words", 1, 0, "Words");
    view->showFullScreen();
    return application->exec();
}

```

### counter.h

```cpp
#ifndef COUNTER_H
#define COUNTER_H

#include <QDebug>
#include <QObject>

class Counter : public QObject {
    Q_OBJECT
    Q_PROPERTY(int count READ getCount WRITE setCounter NOTIFY countChanged)

private:
    int count;

public:
    Q_INVOKABLE Counter() : QObject() {
        this->count = 0;
    }

    Q_INVOKABLE void increment() {
        this->count++;
        emit countChanged();
    }

    Q_INVOKABLE void reset() {
        this->count = 0;
        emit countChanged();
    }

    int getCount() {
        return this->count;
    }

    void setCounter(int counter) {
        this->count = counter;
        emit countChanged();
    }

public slots:
    void print() {
        qDebug() << "Count: " << this->count;
    }

signals:
    void countChanged();
};

#endif // COUNTER_H

```

### words.h

```cpp
#ifndef WORDS_H
#define WORDS_H

#include <QObject>
#include <QString>
#include <QStringList>

class Words : public QObject {
  Q_OBJECT
  Q_PROPERTY(QString list READ string NOTIFY updated)

private:
  QStringList words = QStringList();

public:
  Words() : QObject() {
  }

  Q_INVOKABLE void push(QString string) {
    this->words.append(string.toLower());
    emit updated();
  }

  Q_INVOKABLE void pop() {
    this->words.removeLast();
    emit updated();
  }

  QString string() {
    QString text = this->words.join(", ");
    return text.replace(0, 1, text[0].toUpper());
  }

signals:
  void updated();
};

#endif // WORDS_H

```

### CounterDemo.cpp

```qml
import QtQuick 2.0
import Sailfish.Silica 1.0
import cpp.Counter 1.0

Page {
    id: page

    Counter {
            id: counter
            count: 0
        }
    SilicaFlickable {
        anchors.fill: parent
        contentHeight: column.height

        Column {
            id: column
            width: page.width
            spacing: Theme.paddingLarge

            Label {
                text: counter.count
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: 60
            }

            Button {
                text: "Add"
                anchors.horizontalCenter: parent.horizontalCenter
                onClicked: counter.increment()
            }

            Button {
                text: "Reset"
                anchors.horizontalCenter: parent.horizontalCenter
                onClicked: counter.reset()
            }
        }
    }
}

```

### FirstPage.h

```qml

```

### WordsDemo.h

```qml

```
