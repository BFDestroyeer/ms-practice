\<Титульный лист>

# Постановка задачи

## Цель

Освоить базовые навыки построения пользовательских интерфейсов, позиционирования, отрисовки и перемещения элементов. Научиться анимировать элементы. Научиться создавать диалоги и взаимодействовать с ними.

## Шаги

1. Создать новый проект со стандартной заготовкой приложения.
2. Нарисовать 3 квадрата красного, зелёного и синего цветов следующим образом:

![](media/step01.png)
3. Поместить текст “Квадрат” белого цвета по центру синего квадрата.
4. Нарисовать 5 квадратов с использованием Column и Row следующим образом:
5. Нарисовать те же 5 квадратов с использованием Grid.
6. Сделать из квадрата “A” прямоугольник “B” с использованием объектов Translate, Scale и Rotation
7. Нарисовать квадрат и анимировать его перемещение вниз с увеличением его размера. Документация по анимации доступна по адресу http://doc.qt.io/qt-5/qml-qtquick-animation.html.
8. Реализовать диалог с двумя текстовыми полями, в которые вводятся числа. После нажатия на кнопку “Подтвердить” в консоль выводится сумма чисел. Для преобразования строк к числам использовать функцию parseInt(“42”). Валидацией и обработкой ошибок можно пренебречь.



# Руководство пользователя

1. Запустите приложение **lab03**. На экране отобразится меню с кнопками, чтобы перейти к нужному разделу нажмите соответствующую кнопку.

![](media/user01.png)
2. Шаги со второго по третий представлены в пункте **Basic rectangles**.

![](media/user02.png)
3. Шаги cо четвёртого по пятый представлены в пунктах **Advanced rectangles**, **Grid rectangles**.

![](media/user03.png)
4. Шестой шаг представлен в пункте **Transform rectangles**.

![](media/user04.png) 
5. Седьмой шаг представлен в пункте **Animation rectangles**. При нажатии на квадрат запуститься анимация.

![](media/user05.png) 
6. Шаг 8 представлен в пункте **Dialog**. В открывшемся диалоговом окне, необходимо ввести два числа, как представлено на изображении. После нажатия **Accept** сумма чисел будет выведена в консоль.

![](media/user09.png)

# Руководство программиста

В ходе выполнения практической работы были изучены и использованы, новые компоненты Sailfish SDK а именно:

* **Rectangle** - прямоугольник
* **Text** - контейнер текста
* **Grid** - позиционирование элементов сеткой
* **TextField** - поле ввода текста


### Анимации

* **Animation** - контейнер для анимации

* **ParallelAnimation** - запуск нескольких анимаций

* **PropertyAnimation** - анимация, меняющая свойства объекта
  
* **Transform** - изменение положения объекта

### Диалоги

* **Dialog** - базовый диалог
* **DialogHeader** – заголовок диалога


# Приложение

### AdvancedRectangles.qml

```qml
import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    property int rectangleSize: 200

    Row {
        spacing: 10

        Column {
            spacing: 10

            Rectangle {
                height: page.rectangleSize
                width: page.rectangleSize
                color: "red"
            }

            Rectangle {
                height: page.rectangleSize
                width: page.rectangleSize
                color: "magenta"
            }
        }

        Column {
            spacing: 10

            Rectangle {
                height: page.rectangleSize
                width: page.rectangleSize
                color: "green"
            }
        }

        Column {
            spacing: 10

            Rectangle {
                height: page.rectangleSize
                width: page.rectangleSize
                color: "blue"
            }
            Rectangle {
                height: page.rectangleSize
                width: page.rectangleSize
                color: "black"
            }
        }
    }


}

```

### AnimationRectangles.qml

```qml
import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    property int rectangleSize: 200

    Rectangle {
        id: rectangle
        height: page.rectangleSize
        width: page.rectangleSize
        color: "red"

        anchors.horizontalCenter: parent.horizontalCenter

        ParallelAnimation {
            id: animation
            PropertyAnimation {
                target: rectangle
                properties: "scale"
                from: 1.0
                to: 2.0
                duration: 2000
            }
            PropertyAnimation {
                target: rectangle
                properties: "y"
                from: 0
                to: page.height
                duration: 2000
            }
        }
    }
    onClicked: animation.restart()
}

```

### BasicRectangles.qml

```qml
import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    property int rectangleSize: 200

    Rectangle {
        id: rectangleRed

        height: page.rectangleSize
        width: page.rectangleSize
        color: "red"
    }

    Rectangle {
        id: rectangleGreen

        height: page.rectangleSize
        width: page.rectangleSize
        color: "green"

        anchors.left: rectangleRed.right
        anchors.verticalCenter: rectangleRed.bottom
    }

    Rectangle {
        id: rectangleBlue

        height: page.rectangleSize
        width: page.rectangleSize
        color: "blue"

        anchors.verticalCenter: rectangleGreen.top
        anchors.horizontalCenter: rectangleGreen.right

        Text {
            text: "Квадрат"
            color: "white"

            anchors.centerIn: parent
        }
    }
}

```

### GridRectangles.qml

```qml
import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    property int rectangleSize: 200

    Grid {
        spacing: 10
        columns: 3
        rows: 2

        Rectangle {
            height: page.rectangleSize
            width: page.rectangleSize
            color: "red"
        }

        Rectangle {
            height: page.rectangleSize
            width: page.rectangleSize
            color: "green"
        }

        Rectangle {
            height: page.rectangleSize
            width: page.rectangleSize
            color: "blue"
        }

        Rectangle {
            height: page.rectangleSize
            width: page.rectangleSize
            color: "magenta"
        }

        Item {
            height: page.rectangleSize
            width: page.rectangleSize
        }

        Rectangle {
            height: page.rectangleSize
            width: page.rectangleSize
            color: "black"
        }
    }
}

```

### MainPage.qml

```qml
import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    Column {
        anchors.fill: parent

        Button {
            text: "Basic rectangles"
            anchors.horizontalCenter: parent.horizontalCenter
            onClicked: {
                pageStack.animatorPush(Qt.resolvedUrl("BasicRectangles.qml"))
            }
        }

        Button {
            text: "Advanced rectangles"
            anchors.horizontalCenter: parent.horizontalCenter
            onClicked: {
                pageStack.animatorPush(Qt.resolvedUrl("AdvancedRectangles.qml"))
            }
        }

        Button {
            text: "Grid rectangles"
            anchors.horizontalCenter: parent.horizontalCenter
            onClicked: {
                pageStack.animatorPush(Qt.resolvedUrl("GridRectangles.qml"))
            }
        }

        Button {
            text: "Transforms rectangles"
            anchors.horizontalCenter: parent.horizontalCenter
            onClicked: {
                pageStack.animatorPush(Qt.resolvedUrl("TransformRectangles.qml"))
            }
        }

        Button {
            text: "Animation rectangles"
            anchors.horizontalCenter: parent.horizontalCenter
            onClicked: {
                pageStack.animatorPush(Qt.resolvedUrl("AnimationRectangles.qml"))
            }
        }

        Button {
            text: "Dialog"
            anchors.horizontalCenter: parent.horizontalCenter
            onClicked: {
                dialog.open()
            }
        }

        Dialog {
            id: dialog
            DialogHeader {}

            Row {
                anchors.top: parent.top
                anchors.topMargin: 200

                TextField {
                    id: textFieldA

                    placeholderText: "0"
                    width: 300
                }

                TextField {
                    id: textFieldB

                    placeholderText: "0"
                    width: 300

                    anchors.left: textFieldA.right
                }
            }

            onAccepted: {
                console.log("SUMM: " + (parseInt(textFieldA.text) + parseInt(textFieldB.text)))
            }
        }
    }
}

```

### TransformRectangles.qml

```qml
import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    property int rectangleSize: 200

    Rectangle {
        height: page.rectangleSize
        width: page.rectangleSize
        color: "black"

        Text {
            text: "A"
            color: "white"

            anchors.centerIn: parent
        }
    }

    Rectangle {
        height: page.rectangleSize
        width: page.rectangleSize
        color: "black"

        Text {
            text: "B"
            color: "white"

            anchors.centerIn: parent
        }

        transform: [
            Scale {
                xScale: 0.5
            },
            Rotation {
                angle: 45
            },
            Translate {
                x: page.rectangleSize * 2
                y: page.rectangleSize * 0.1
            }
        ]
    }

}

```
