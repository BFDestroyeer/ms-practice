@echo off

del /q output\*.docx

pandoc --from=gfm --to=docx --reference-doc=reference.docx --resource-path=lab02 -o output\lab02.docx lab02\report.md
pandoc --from=gfm --to=docx --reference-doc=reference.docx --resource-path=lab03 -o output\lab03.docx lab03\report.md
pandoc --from=gfm --to=docx --reference-doc=reference.docx --resource-path=lab04 -o output\lab04.docx lab04\report.md
pandoc --from=gfm --to=docx --reference-doc=reference.docx --resource-path=lab05 -o output\lab05.docx lab05\report.md
pandoc --from=gfm --to=docx --reference-doc=reference.docx --resource-path=lab06 -o output\lab06.docx lab06\report.md
pandoc --from=gfm --to=docx --reference-doc=reference.docx --resource-path=lab07 -o output\lab07.docx lab07\report.md
pandoc --from=gfm --to=docx --reference-doc=reference.docx --resource-path=lab08 -o output\lab08.docx lab08\report.md

explorer.exe output
